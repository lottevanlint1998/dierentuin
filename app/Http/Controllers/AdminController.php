<?php

namespace App\Http\Controllers;

use App\Dierenshow;
use App\Registration;

use Illuminate\Http\Request;
use App\Http\Requests\saveAdminRequest;

class AdminController extends Controller
{
    public function index()
    {
        $dierenshows = Dierenshow::all();
        return view('admin.index', compact('dierenshows'));
    }
    public function registrations()
    {
        $registrations = Registration::all();
        return view('admin.registrations', compact('registrations'));
    }
    public function create()
    {
        $dierenshows = new Dierenshow;
        return view('admin.create', compact('dierenshows'));
    }

    public function store(Request $request)
    {

        $dierenshows = new Dierenshow;
        $dierenshows->title = $request->title;
        $dierenshows->info = $request->info;
        $dierenshows->date = $request->date;
        $dierenshows->place = $request->place;
        $dierenshows->image = $request->image;

        $dierenshows->save();

        return \redirect()->route('admin.index');
    }
    public function edit(Dierenshow $dierenshows)
    {
        return view('admin.edit', compact('dierenshows'));
    }
    public function update(Request $request, Dierenshow $dierenshows)
    {
        $request->validate([
            'title' => 'required',
            'info' => 'required',
            'date' => 'required',
            'place' => 'required',


        ]);
        $dierenshows->title = $request->title;
        $dierenshows->info = $request->info;
        $dierenshows->date = $request->date;
        $dierenshows->place = $request->place;
        $dierenshows->image = $request->image;

        $dierenshows->save();

        return \redirect()->route('admin.index');
    }
    public function delete(Dierenshow $dierenshows)
    {
        $dierenshows->delete();
        return \redirect()->route('admin.index');
    }
}