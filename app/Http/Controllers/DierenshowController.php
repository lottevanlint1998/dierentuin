<?php

namespace App\Http\Controllers;

use App\Dierenshow;
use App\Registration;
use Illuminate\Http\Request;

class DierenshowController extends Controller
{
    public function index()
    {
        $dierenshows = Dierenshow::all();
        return view('shows.index', compact('dierenshows'));
    }

    public function show($id)
    {
        $dierenshows = Dierenshow::where('id', $id)->first();
        return view('shows.show', compact('dierenshows'));
    }

    public function create()
    {
        $registration = new Registration;
        $dierenshows = Dierenshow::all();
        return view('shows.create', compact('registration', 'dierenshows'));
    }

    public function store(Request $request)
    {
        $registration = new Registration;
        $registration->showname = $request->showname;
        $registration->name = $request->name;
        $registration->email = $request->email;
        $registration->save();

        return \redirect()->route('shows.index');
    }


}
