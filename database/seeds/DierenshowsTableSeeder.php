<?php

use App\Show;
use App\Dierenshow;
use Illuminate\Database\Seeder;

class DierenshowsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dierenshows')->truncate();

        $dierenshow = new Dierenshow;
        $dierenshow->title = "Zeehonden show";
        $dierenshow->date = "2020-05-20";
        $dierenshow->place = "Zeehondenbad";
        $dierenshow->info = "Deze show is heel leuk, de zeehondjes zwemmen steeds in het rond en doen een aantal trucjes. Ook geven we ze vis tijdens de show.";
        $dierenshow->image = "https://images.pexels.com/photos/185032/pexels-photo-185032.jpeg?cs=srgb&dl=dewgong-on-body-of-water-185032.jpg&fm=jpg";
        $dierenshow->save();

        $dierenshow = new Dierenshow;
        $dierenshow->title = "dolfijnen show";
        $dierenshow->date = "2020-05-27";
        $dierenshow->place = "Dolfijnenbad";
        $dierenshow->info = "Super leuke show! De dolfijnen kennen een heel deel trucjes. Ook krijgen ze eten tijdens de show, voor ze iets krijgen moeten ze steeds een trucje doen.";
        $dierenshow->image = "https://images.pexels.com/photos/162079/dolphin-sea-marine-mammals-wise-162079.jpeg?cs=srgb&dl=dolphin-s-head-in-the-surface-162079.jpg&fm=jpg";
        $dierenshow->save();

        $dierenshow = new Dierenshow;
        $dierenshow->title = "vogel show";
        $dierenshow->date = "2020-06-01";
        $dierenshow->place = "vogelplein";
        $dierenshow->info = "De vogelshow is heel gevarieerd, de vogels vliegen rond en als we ze roepen met een speciaal handgebaar komen ze op onze speciale handschoen zitten.";
        $dierenshow->image = "https://images.pexels.com/photos/151511/pexels-photo-151511.jpeg?cs=srgb&dl=white-and-brown-bird-151511.jpg&fm=jpg";
        $dierenshow->save();
    }
}
