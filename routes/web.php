<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//index
Route::get('/', 'DierenshowController@index')->name('shows.index');

//client

Route::get('shows/create', 'DierenshowController@create')->name('shows.create');
Route::post('shows/create', 'DierenshowController@store')->name('shows.store');
Route::get('/shows/{id}', 'DierenshowController@show')->name('shows.show');

//Admin
Route::get('admin', 'AdminController@index')->name('admin.index');
Route::get('admin/create', 'AdminController@create')->name('admin.create');
Route::post('admin/create', 'AdminController@store')->name('admin.store');
Route::get('admin/edit/{dierenshows}', 'AdminController@edit')->name('admin.edit');
Route::put('admin/edit/{dierenshows}', 'AdminController@update')->name('admin.update');
Route::get('admin/delete/{dierenshows}', 'AdminController@delete')->name('admin.delete');
Route::get('admin/registrations', 'AdminController@registrations')->name('admin.registrations');
