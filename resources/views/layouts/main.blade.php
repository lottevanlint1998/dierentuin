<!DOCTYPE html>
<html lang="en">

<head>
@section('meta')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> 
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
@show
    <title>dierentuin site - @yield('title')</title>
</head>

<body>
<header style="background-color:#53AFA2; color: white; font-size: 3em; text-align: center;">
    <p>Dierentuin</p>
</header>
        <div class="container">

		<section>
			@yield('content')
		</section>
	</div>
        </div>
    </body>

</html>