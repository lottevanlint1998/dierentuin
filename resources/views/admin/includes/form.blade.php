@csrf
<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">Title</span>
</div>
    <input type="text" class="form-control" placeholder="Title" name="title" value="{{ old('title', $dierenshows->title) }}">
    @error('title')
    <br><span style="color:red;">{{$message}}</span>
    @enderror
</div>
<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">Info</span>
</div>
    <input type="text" class="form-control" placeholder="Info" name="info" value="{{ old('info', $dierenshows->info) }}">
    @error('info')
    <br><span style="color:red;">{{$message}}</span>
    @enderror
</div>
<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">date</span>
</div>
    <input type="text" class="form-control" placeholder="Date" name="date" value="{{ old('date', $dierenshows->date) }}">
    @error('date')
    <br><span style="color:red;">{{$message}}</span>
    @enderror
</div>
<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">Place</span>
</div>
    <input type="text" class="form-control" placeholder="Place" name="place" value="{{ old('place', $dierenshows->place) }}">
    @error('place')
    <br><span style="color:red;">{{$message}}</span>
    @enderror
</div>
<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">Image</span>
</div>
    <input type="text" class="form-control" placeholder="image" name="image" value="{{ old('image', $dierenshows->image) }}">
    @error('image')
    <br><span style="color:red;">{{$message}}</span>
    @enderror
</div>