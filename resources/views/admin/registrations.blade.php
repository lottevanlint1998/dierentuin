@extends('layouts.main')

@section('title', 'Admin')
		
@section('content')
<a style="margin-top: 20px; margin-bottom: 20px;" class="btn btn-primary" href="{{ route('admin.index') }}">Back</a>


	<h1>Registrations</h1>
		
	<table class="table">
		<thead>
			<tr>
			<th scope="col">#</th>
			<th scope="col">Show</th>
			<th scope="col">Name</th>
			<th scope="col">Email</th>
			</tr> 
		</thead>
		<tbody>
	@foreach($registrations as $registration)
			<tr>
				<th scope="row">{{ $registration->id }}</th>
				<td>{{ $registration->showname }}</td>
				<td>{{ $registration->name }}</td>
                <td>{{ $registration->email }}</td>
			</tr>
	@endforeach
		</tbody>
	</table>
	
@endsection