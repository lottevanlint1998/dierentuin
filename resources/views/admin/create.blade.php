@extends('layouts.main')

@section('title', 'Dierenshows')
		
@section('content')
<a style="margin: 20px; margin-left: 0;" class="btn btn-primary" href="{{ route('admin.index') }}">Back</a>
		<h1>Create show</h1>
		
            <form method="post" action="{{ route('admin.store') }}">
			@include('admin.includes.form')

			
			<input class="btn btn-primary" type="submit" value="Add new show">

		</form>
@endsection