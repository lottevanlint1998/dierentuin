@extends('layouts.main')

@section('title', 'Dierentuin - Edit show')
		
@section('content')
		<h1>Edit show</h1>

		<form method="post" action="{{ route('admin.update', $dierenshows->id) }}">
			@method('PUT')
			@include('admin.includes.form')
			<p>
				<input class="btn btn-primary" type="submit" value="Update show">
			</p>
		</form>
@endsection