@extends('layouts.main')

@section('title', 'Admin')
		
@section('content')

	<h1>Admin</h1>
		
	<table class="table">
                   <a style="margin: 20px; margin-left: 0;" class="btn btn-primary" href="{{ route('admin.create') }}">Create new show</a>
				   <a style="margin: 20px;" class="btn btn-primary" href="{{ route('admin.registrations') }}">Watch all registrations</a>

		<thead>
			<tr>
			<th scope="col">#</th>
			<th scope="col">Title</th>
			<th scope="col">Date</th>
			<th scope="col">Place</th>
			<th scope="col">Info</th>
			<th scope="col">Image</th>
			<th scope="col">Acties</th>
			</tr> 
		</thead>
		<tbody>
	@foreach($dierenshows as $dierenshow)
			<tr>
				<th scope="row">{{ $dierenshow->id }}</th>
				<td>{{ $dierenshow->title }}</td>
				<td>{{ $dierenshow->date }}</td>
                <td>{{ $dierenshow->place }}</td>
                <td>{{ $dierenshow->info }}</td>
				<td><img width="100" src="{{ $dierenshow->image }}" alt="{{ $dierenshow->title }}"></td>
				<td><a  href="{{ route('admin.edit', $dierenshow->id) }}">Edit</a></td>			
                <td><a  href="{{ route('admin.delete', $dierenshow->id) }}">Delete</a></td>				

			</tr>
	@endforeach
		</tbody>
	</table>
	
@endsection