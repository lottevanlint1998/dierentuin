@extends('layouts.main')

@section('title', 'Dierenshows')
		
@section('content')

	<a style="margin: 20px; margin-left: 0;" class="btn btn-primary" href="{{ route('shows.create') }}">Register for a show</a>

	<h1>Alle dierenshows</h1>
		
	<table class="table">
		<thead>
			<tr>
			<th scope="col">#</th>
			<th scope="col">Title</th>
			<th scope="col">Date</th>
			<th scope="col">Place</th>
			<th scope="col">Acties</th>
			</tr>
		</thead>
		<tbody>
	@foreach($dierenshows as $dierenshow)
			<tr>
				<th scope="row">{{ $dierenshow->id }}</th>
				<td>{{ $dierenshow->title }}</td>
				<td>{{ $dierenshow->date }}</td>
                <td>{{ $dierenshow->place }}</td>
				<td><a  href="{{ route('shows.show', $dierenshow->id) }}">Lees meer</a></td>
			</tr>
	@endforeach
		</tbody>
	</table>

@endsection