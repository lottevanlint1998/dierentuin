
@extends('layouts.main')

@section('title', 'Dierenshows')
		
@section('content')
<a style="margin: 20px; margin-left: 0;" class="btn btn-primary" href="{{ route('shows.index') }}">Back</a>
<h1>Schrijf je in voor een Dierenshow</h1>

<h1>Your choice: </h1>
<form method="post" action="{{ route('shows.store') }}">
@csrf

<div class="input-group mb-3">
<div class="input-group-prepend">
    <label class="input-group-text" for="inputGroupSelect01">Shows</label>
</div>
<select name="showname" class="custom-select" id="inputGroupSelect01">
    @foreach($dierenshows as $dierenshow)
            <option name="showname" value="{{ $dierenshow->title }}">{{ $dierenshow->title }}</option>
    @endforeach
</select>
</div>


<h1>Your personal info: </h1>

<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">Name</span>
</div>
    <input type="text" class="form-control" placeholder="Name" name="name">
    @error('name')
    <br><span style="color:red;">{{$message}}</span>
    @enderror
</div>
<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">@</span>
</div>
    <input type="text" class="form-control" placeholder="Email" name="email">
    @error('email')
    <br><span style="color:red;">{{$message}}</span>
    @enderror
</div>
<input class="btn btn-primary" type="submit" value="Register">
</form>	

@endsection