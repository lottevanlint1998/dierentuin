@extends('layouts.main')

@section('title', 'Title: ' . $dierenshows->title)
		
@section('content')
<a style="margin: 20px; margin-left: 0;" class="btn btn-primary" href="{{ route('shows.index') }}">Back</a>
		<h2>{{ $dierenshows->title }}</h2>
		<p><b>{{ $dierenshows->title }}</b> </p>
		<p><b>Info:</b> {{ $dierenshows->info }}</p>
		<img width="500" src="{{ $dierenshows->image }}" alt="{{ $dierenshows->info }}">
@endsection